# Artisan Scaffold

[![Latest Version on Packagist](https://img.shields.io/packagist/v/martinshaw/artisan-scaffold.svg?style=flat-square)](https://packagist.org/packages/martinshaw/artisan-scaffold)
[![Total Downloads](https://img.shields.io/packagist/dt/martinshaw/artisan-scaffold.svg?style=flat-square)](https://packagist.org/packages/martinshaw/artisan-scaffold)

This is where your description should go. Try and limit it to a paragraph or two, and maybe throw in a mention of what PSRs you support to avoid any confusion with users and contributors.

## Installation

You can install the package via composer:

```bash
composer require martinshaw/artisan-scaffold
```

## Usage

For example, the 4 commands described below will generate basic functionality for a blog site:

```bash
artisan scaffold tags name:text color:color.optional has_many:posts
artisan scaffold comments content:textarea is_visible:boolean=true has_one:post has_one:user
artisan scaffold posts title:slug.unique content:textarea thumbnail_image:image has_one:user has_many:comments has_many:tags --paginated
artisan migrate
```

Parts of the command signature:

* Resource name in plural or singular form
* Zero or more properties of the resource (Property in model, Field in forms and validation, Column in migration):
    * Name of the property
    * `:` character to denote separation between name and type
    * Type of the property (includes all Migration column types and additional convenience types as listed in the 'Reference' section below)
    * *optional* `.` character to denote separation between type and one or more modifier
        * Chained modifier type (as listed in the 'Reference' section below) which describes validation and/or migration constraints
    * *optional* `=` character to denote separation between previous parts, and the default value
        * Default value for the property with data type inferred from property type. Surround multi-word string values with `\'`.
* *optional* Zero or more relationships of the resource
    * Type of relationship (as listed in the 'Reference' section below)
    * `:` character to denote separation between type of relationship, and the name of the related resource
    * Name of the related resource in plural or singular form
* *optional* Zero or more additional flags (as listed in the 'Reference' section below) which describes additional features to be implemented when generating files
    * `--` characters denote an additional flag in the form of a traditional CLI flag
    * Name of the additional flag
    
### Testing

```bash
composer test
```

### Changelog

Please see [CHANGELOG](CHANGELOG.md) for more information what has changed recently.

## Contributing

Please see [CONTRIBUTING](CONTRIBUTING.md) for details.

### Security

If you discover any security related issues, please email developer@martinshaw.co instead of using the issue tracker.

## Credits

-   [Martin Shaw](https://gitlab.com/martinshaw)
-   [All Contributors](../../contributors)

## License

The MIT License (MIT). Please see [License File](LICENSE.md) for more information.
