<?php

namespace Martinshaw\ArtisanScaffold;

use Illuminate\Support\Facades\Facade;

/**
 * @see \Martinshaw\ArtisanScaffold\Skeleton\SkeletonClass
 */
class ArtisanScaffoldFacade extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'artisan-scaffold';
    }
}
