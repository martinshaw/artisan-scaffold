<?php
namespace Martinshaw\ArtisanScaffold\Console\Property;

use Martinshaw\ArtisanScaffold\Console\Commands\ArtisanScaffoldCommand;

class ArtisanScaffoldProperty
{
    protected ArtisanScaffoldCommand $context;

    protected string $input;
    protected string $name;
    protected string $type;
    protected array $modifiers = [];

    const InputRegex = '/^(?\'name\'[\w]+)\:(?\'type\'[\w]+)(?:[\.](?\'modifiers\'[\w\.]+)){0,1}/';

    /**
     * ArtisanScaffoldProperty constructor.
     * @param ArtisanScaffoldCommand $context
     * @param string $input
     */
    public function __construct (ArtisanScaffoldCommand $context, string $input) {
        $this->context = $context;

        preg_match_all(static::InputRegex, $input, $matches, PREG_SET_ORDER, 0);
        $matches = array_map(function ($match) { return array_filter($match, 'is_string', ARRAY_FILTER_USE_KEY); }, $matches);
        $this->input = $input;

        if (empty($matches[0]['name'])) {
            $this->context->error('One of the requested properties does not have a valid property name');
            exit(1);
        }

        if (empty($matches[0]['type'])) {
            $this->context->error('The requested property \'' . $matches[0]['name'] . '\' does not have a valid property type');
            exit(1);
        }

        $this->name = $matches[0]['name'];
        $this->type = $matches[0]['type'];
        $this->modifiers = explode('.', $matches[0]['modifiers'] ?? '');
    }

    /**
     * @return string
     */
    public function getName () : string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getType () : string
    {
        return $this->type;
    }

    /**
     * @return array
     */
    public function getModifiers () : array
    {
        return $this->modifiers;
    }
}