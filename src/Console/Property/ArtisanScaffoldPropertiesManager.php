<?php
namespace Martinshaw\ArtisanScaffold\Console\Property;

use Martinshaw\ArtisanScaffold\Console\Commands\ArtisanScaffoldCommand;

class ArtisanScaffoldPropertiesManager
{
    protected ArtisanScaffoldCommand $context;

    protected array $propertyTypes = [];
    protected array $propertyTypeNames = [];
    protected array $propertyInput = [];
    protected array $properties = [];

    /**
     * ArtisanScaffoldPropertiesManager constructor.
     * @param ArtisanScaffoldCommand $context
     * @param array $propertyTypes
     * @param array $propertyInput
     */
    public function __construct (ArtisanScaffoldCommand $context, array $propertyTypes = [], array $propertyInput = [])
    {
        $this->context = $context;
        $this->propertyInput = $propertyInput;

        $this->propertyTypes = $propertyTypes;
        $this->propertyTypeNames = array_keys($this->propertyTypes);
        $this->properties = $this->mapInputToPropertyTypeInstances($propertyInput);
        $propCount = count($this->properties);
        $this->context->info("Configured {$propCount} new properties for the new resource ...\n");
    }

    /**
     * @param array $inputs
     * @return array
     */
    protected function mapInputToPropertyTypeInstances (array $inputs = []): array
    {
        $self = $this;
        return array_map(function (string $input) use ($self) {
            $property = new ArtisanScaffoldProperty($self->context, $input);
            $typeClass = $self->getPropertyTypeClassByName($property->getType());
            return new $typeClass($property);
        }, $inputs);
    }

    /**
     * @param string $typeName
     * @return string
     */
    protected function getPropertyTypeClassByName (string $typeName) : string
    {
        if (isset($this->propertyTypes[$typeName]) === false) {
            $this->context->error('There is no property type named \'' . $typeName . '\' in the builder\'s getPropertyTypes array. You may resolve this by referencing the builder package\'s documentation or visit the Artisan Scaffold website.');
            exit(1);
        }
        if (class_exists($this->propertyTypes[$typeName]) === false) {
            $this->context->error('There is no class assigned to the property type named \'' . $typeName . '\' in the builder\'s getPropertyTypes array. You may resolve this by referencing the builder package\'s documentation or visit the Artisan Scaffold website.');
            exit(1);
        }

        return $this->propertyTypes[$typeName];
    }

    /**
     * @return array
     */
    public function getProperties () : array
    {
        return $this->properties;
    }
}