<?php
namespace Martinshaw\ArtisanScaffold\Console\Builder;

use Illuminate\Support\Str;

trait HasPathHelperMethods
{
    /**
     * @return string
     */
    protected function getMigrationPath () : string
    {
        $base = base_path() . '/database/migrations';
        $scan = scandir($base) ?? [];

        $path = '';
        foreach ($scan as $fileName) {
            if (strpos($fileName, 'create_'.Str::snake($this->context->getResourceNamePlural()).'_table.php') !== false) {
                $path = $base . '/' . $fileName;
                break;
            }
        }

        if (empty($path)) {
            $this->context->error('We are unable to find the generated migration file');
            exit(1);
        }
        return $path;
    }

    /**
     * @return string
     */
    protected function getWebControllerPath () : string
    {
        $base = base_path() . '/app/Http/Controllers';
        $path = $base . '/' . Str::studly($this->context->getResourceNameSingular()) . 'Controller.php';

        if (file_exists($path) === false) {
            $this->context->error('We are unable to find the generated web controller file');
            exit(1);
        }
        return $path;
    }

    /**
     * @return string
     */
    protected function getModelPath () : string
    {
        $base = base_path() . '/app/Models';
        $path = $base . '/' . Str::studly($this->context->getResourceNameSingular()) . '.php';

        if (file_exists($path) === false) {
            $this->context->error('We are unable to find the generated model file');
            exit(1);
        }
        return $path;
    }
}