<?php
namespace Martinshaw\ArtisanScaffold\Console\Builder;

use Illuminate\Support\Str;
use Martinshaw\ArtisanScaffold\Console\Commands\ArtisanScaffoldCommand;
use Martinshaw\ArtisanScaffold\Console\Property\ArtisanScaffoldPropertiesManager;
use Martinshaw\ArtisanScaffold\Console\PropertyTypes\PropertyType;

abstract class ArtisanScaffoldBuilder
{
    use HasPathHelperMethods, HasFileOperationHelperMethods, HasArtisanHelperMethods;

    /**
     * @var ArtisanScaffoldCommand
     */
    protected ArtisanScaffoldCommand $context;

    /**
     * @var ArtisanScaffoldPropertiesManager
     */
    protected ArtisanScaffoldPropertiesManager $propertiesManager;

    /**
     * @var array|null
     */
    protected ?array $cachedDefaultTemplateData = null;

    /**
     * ArtisanScaffoldBuilder constructor.
     * @param ArtisanScaffoldCommand $context
     */
    public function __construct (ArtisanScaffoldCommand $context)
    {
        $this->context = $context;
    }

    /**
     * @param ArtisanScaffoldPropertiesManager $propertiesManager
     * @param array $flags
     */
    abstract function run (ArtisanScaffoldPropertiesManager $propertiesManager, array $flags) : void;

    /**
     * @param array $flags
     */
    abstract function revert (array $flags) : void;

    /**
     * Should return an array keyed by property type names assigned to the appropriate property type class namespaced name
     * @return array
     */
    abstract function getPropertyTypes () : array;

    /**
     * @return string
     */
    public function getMigrationColumnSnippets(): string
    {
        return implode(
            "\n",
            array_map(function (PropertyType $property) {
                return $property->getMigrationColumnSnippet();
            }, $this->propertiesManager->getProperties())
        );
    }
}