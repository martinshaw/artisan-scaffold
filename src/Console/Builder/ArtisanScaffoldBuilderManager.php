<?php
namespace Martinshaw\ArtisanScaffold\Console\Builder;

use Martinshaw\ArtisanScaffold\Console\Commands\ArtisanScaffoldCommand;

class ArtisanScaffoldBuilderManager
{
    protected ArtisanScaffoldCommand $context;

    protected string $name;
    protected array $builders = [];
    protected array $builderNames = [];

    protected ArtisanScaffoldBuilder $builder;

    /**
     * ArtisanScaffoldBuilderManager constructor.
     * @param ArtisanScaffoldCommand $context
     * @param string $name
     */
    public function __construct (ArtisanScaffoldCommand $context, string $name)
    {
        $this->context = $context;
        $this->name = $name;

        $this->loadBuildersFromConfig();
        $this->context->info("Using '{$this->name}' builder to generate new content...\n");
        $class = $this->getBuilderClassByName($this->name);
        $this->builder = new $class($this->context);
    }

    protected function loadBuildersFromConfig () : void
    {
        $builders = config('artisan-scaffold.builders');
        $this->builders = is_array($builders) ? $builders : [];
        $this->builderNames = array_keys($this->builders);
    }

    /**
     * @param string $name
     * @return string
     */
    protected function getBuilderClassByName (string $name) : string
    {
        if (isset($this->builders[$name]) === false) {
            $this->context->error('There is no builder named \'' . $name . '\' in your artisan-scaffold.php config file. You may resolve this by running `php artisan vendor:publish`.');
            exit(1);
        }
        if (class_exists($this->builders[$name]) === false) {
            $this->context->error('There is no class assigned to the builder named \'' . $name . '\' in your artisan-scaffold.php config file. You may resolve this by running `php artisan vendor:publish`.');
            exit(1);
        }

        return $this->builders[$name];
    }

    /**
     * @return ArtisanScaffoldBuilder
     */
    public function getBuilder () : ArtisanScaffoldBuilder
    {
        return $this->builder;
    }
}