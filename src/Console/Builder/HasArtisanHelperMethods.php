<?php
namespace Martinshaw\ArtisanScaffold\Console\Builder;

use Illuminate\Support\Str;

trait HasArtisanHelperMethods
{
    /**
     * @param bool $controller
     * @param bool $migration
     * @param bool $seed
     * @param bool $factory
     * @param bool $pivot
     * @param bool $resource
     * @param bool $api
     * @return int
     */
    protected function artisanMake (
        bool $controller = true,
        bool $migration = true,
        bool $seed = false,
        bool $factory = false,
        bool $pivot = false,
        bool $resource = false,
        bool $api = false
    ) : int {
        $params = [];
        $params['name'] = Str::studly($this->context->getResourceNameSingular());
        if ($controller) { $params['--controller'] = true; }
        if ($migration) { $params['--migration'] = true; }
        if ($seed) { $params['--seed'] = true; }
        if ($factory) { $params['--factory'] = true; }
        if ($pivot) { $params['--pivot'] = true; }
        if ($resource) { $params['--resource'] = true; }
        if ($api) { $params['--api'] = true; }

        $this->context->info("Running artisan make:model with " . implode(', ', array_map(function ($item) { return str_replace('--', '', $item); }, array_keys($params))) . " options ... \n");

        $code = $this->context->call('make:model', $params);
        if ($code !== 0) {
            $this->context->error('An error occurred when attempting to run make:model with relevant parameters. Returned code: ' . $code);
            exit(1);
        }

//        $this->context->info("Found generated migration file at " . $this->getMigrationPath());
//        $this->context->info("Found generated web controller file at " . $this->getWebControllerPath());
//        $this->context->info("Found generated model file at " . $this->getModelPath());

        $this->context->line('');
        return $code === 0;
    }

    protected function composerDumpAutoload () : void
    {
        $this->context->info('Running \'composer dump-autoload\' to include namespace changes ...');
        exec("composer dump-autoload -o -d " . base_path());
    }
}