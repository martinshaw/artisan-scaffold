<?php
namespace Martinshaw\ArtisanScaffold\Console\Builder;

use Illuminate\Support\Str;

trait HasFileOperationHelperMethods
{
    /**
     * @param string $filePath
     * @param string $contents
     * @param bool $silent
     * @return false|int
     */
    protected function writeFile (string $filePath, string $contents = '', bool $silent = false)
    {
        $writer = file_put_contents($filePath, $contents);
        if ($writer === false) {
            $this->context->error('✗ An error occurred when attempting to write to file: ' . $filePath);
        } else if ($silent === false) {
            $this->context->info('✓ ' . $filePath);
        }
        return $writer;
    }

    /**
     * @param string $filePath
     * @param bool $silent
     * @return false|int
     */
    protected function readFile (string $filePath, bool $silent = false)
    {
        $reader = file_get_contents($filePath);
        if ($reader === false) {
            $this->context->error('✗ An error occurred when attempting to read from file: ' . $filePath);
        } else if ($silent === false) {
            $this->context->info('✓ ' . $filePath);
        }
        return $reader;
    }

    /**
     * @param string $filePath
     * @param string $contents
     * @return false|int
     */
    protected function prependFile (string $filePath, string $contents = '')
    {
        $currentContent = file_get_contents($filePath);
        if ($currentContent === false) { return false; }

        $writer = file_put_contents($filePath, $contents . PHP_EOL . $currentContent);
        if ($writer === false) {
            $this->context->error('✗ An error occurred when attempting to prepend to file: ' . $filePath);
        } else {
            $this->context->info('✓ ' . $filePath);
        }
        return $writer;
    }

    /**
     * @param string $filePath
     * @param string $contents
     * @return false|int
     */
    protected function appendFile (string $filePath, string $contents = '')
    {
        $currentContent = file_get_contents($filePath);
        if ($currentContent === false) { return false; }

        $writer = file_put_contents($filePath, $currentContent . PHP_EOL . $contents);
        if ($writer === false) {
            $this->context->error('✗ An error occurred when attempting to append to file: ' . $filePath);
        } else {
            $this->context->info('✓ ' . $filePath);
        }
        return $writer;
    }

    /**
     * @param string|array $filePaths
     */
    protected function deleteFiles ($filePaths)
    {
        if (is_string($filePaths)) { $filePaths = [$filePaths]; }
        foreach ($filePaths as $filePath) {
            if (file_exists($filePath) === false) {
                $this->context->error('✗ Unable to find file for deletion: ' . $filePath);
                continue;
            }
            $deleter = @unlink($filePath);
            if ($deleter === false) {
                $this->context->error('✗ An error occurred when attempting to delete file: ' . $filePath);
                continue;
            }
            $this->context->info('✓ ' . $filePath);
        }
    }

    /**
     * @param string|array $filePaths
     */
    protected function deleteDirectories ($filePaths)
    {
        if (is_string($filePaths)) { $filePaths = [$filePaths]; }
        foreach ($filePaths as $filePath) {
            if (file_exists($filePath) === false) {
                $this->context->error('✗ Unable to find directory for deletion: ' . $filePath);
                continue;
            }

            $this->deleteFiles(
                glob(mb_substr($filePath, -1) === '/' ? $filePath . '*' : $filePath . '/*')
            );

            $dirDeleter = @rmdir($filePath);
            if ($dirDeleter === false) {
                $this->context->error('✗ An error occurred when attempting to delete directory: ' . $filePath);
                continue;
            }
            $this->context->info('✓ ' . $filePath);
        }
    }

    /**
     * @param string $path
     * @param array $data
     * @return string
     */
    protected function getTemplateFile (string $path, array $data = []) : string
    {
        extract($this->mergeDefaultTemplateData($data), EXTR_OVERWRITE);
        return require_once($path);
    }

    /**
     * @param array $data
     * @return array
     */
    protected function mergeDefaultTemplateData (array $data = []) : array
    {
        if ($this->cachedDefaultTemplateData === null) {
            $firstPropertyName = $this->propertiesManager->getProperties()[0]->getProperty()->getName();

            $this->cachedDefaultTemplateData = [
                'resourceSingularUpper' => Str::studly($this->context->getResourceNameSingular()),
                'resourceSingularLower' => Str::snake($this->context->getResourceNameSingular()),
                'resourcePluralUpper'   => Str::studly($this->context->getResourceNamePlural()),
                'resourcePluralLower'   => Str::snake($this->context->getResourceNamePlural()),
                'firstPropertyName'     => $firstPropertyName,
                'firstPropertyCaption'  => Str::ucfirst(str_replace('_', ' ', Str::snake($firstPropertyName))),
                'properties'            => $this->propertiesManager->getProperties(),
            ];
        }

        return array_merge($this->cachedDefaultTemplateData, $data);
    }
}