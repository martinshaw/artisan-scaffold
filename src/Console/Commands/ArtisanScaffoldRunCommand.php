<?php
namespace Martinshaw\ArtisanScaffold\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Str;
use Martinshaw\ArtisanScaffold\Console\Property\ArtisanScaffoldPropertiesManager;
use Martinshaw\ArtisanScaffold\Console\Builder\ArtisanScaffoldBuilderManager;

class ArtisanScaffoldRunCommand extends ArtisanScaffoldCommand
{
    protected $signature = 'scaffold {resource : Name of the new resource in plural or singular form} {properties* : Properties configuration for the new resource}';
    protected $description = 'Scaffold a new resource';

    public function handle()
    {
        parent::handle();
        $this->builderManager = new ArtisanScaffoldBuilderManager($this, config('artisan-scaffold.default_builder', 'simple'));
        $this->propertyManager = new ArtisanScaffoldPropertiesManager($this, $this->builderManager->getBuilder()->getPropertyTypes(), $this->arguments()['properties']);
        $this->builderManager->getBuilder()->run($this->propertyManager, $this->arguments());
    }
}