<?php
namespace Martinshaw\ArtisanScaffold\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Str;
use Martinshaw\ArtisanScaffold\Console\Builder\ArtisanScaffoldBuilderManager;
use Martinshaw\ArtisanScaffold\Console\Property\ArtisanScaffoldPropertiesManager;

class ArtisanScaffoldCommand extends Command
{
    protected string $resourceNameOriginal;
    protected string $resourceNameSingular;
    protected string $resourceNamePlural;
    protected bool $originalResourceNameIsSingular = false;

    protected ArtisanScaffoldBuilderManager $builderManager;
    protected ArtisanScaffoldPropertiesManager $propertyManager;

    protected function handle ()
    {
        $this->setResourceNameVariants($this->arguments()['resource']);
    }

    /**
     * @param string $resourceName
     */
    protected function setResourceNameVariants (string $resourceName) : void
    {
        $this->resourceNameOriginal = $resourceName;
        $this->resourceNamePlural = Str::plural($this->resourceNameOriginal);
        $this->resourceNameSingular = Str::singular($this->resourceNameOriginal);
        $this->originalResourceNameIsSingular = Str::singular($this->resourceNameOriginal) === $this->resourceNameOriginal;
    }

    /**`
     * @return string
     */
    public function getResourceNameSingular () : string
    {
        return $this->resourceNameSingular;
    }

    /**
     * @return string
     */
    public function getResourceNamePlural () : string
    {
        return $this->resourceNamePlural;
    }
}