<?php
namespace Martinshaw\ArtisanScaffold\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Str;
use Martinshaw\ArtisanScaffold\Console\Builder\ArtisanScaffoldBuilderManager;

class ArtisanScaffoldRevertCommand extends ArtisanScaffoldCommand
{
    protected $signature = 'scaffold:revert {resource : Name of the new resource in plural or singular form}';
    protected $description = 'Revert files generated during historical resource scaffolding';

    public function handle()
    {
        parent::handle();
        $this->builderManager = new ArtisanScaffoldBuilderManager($this, config('artisan-scaffold.default_builder', 'simple'));
        $this->builderManager->getBuilder()->revert($this->arguments());
    }
}