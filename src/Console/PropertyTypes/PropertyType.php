<?php
namespace Martinshaw\ArtisanScaffold\Console\PropertyTypes;

use Martinshaw\ArtisanScaffold\Console\Property\ArtisanScaffoldProperty;

abstract class PropertyType
{
    protected string $propertyTypeName = '';
    protected string $propertyTypeCaption = '';

    protected ArtisanScaffoldProperty $property;

    /**
     * PropertyType constructor.
     * @param string $propertyTypeName
     * @param string $propertyTypeCaption
     * @param ArtisanScaffoldProperty $property
     */
    public function __construct (
        string $propertyTypeName,
        string $propertyTypeCaption,
        ArtisanScaffoldProperty $property
    ) {
        $this->propertyTypeName = $propertyTypeName;
        $this->propertyTypeCaption = $propertyTypeCaption;

        //todo: add modifier existence validation based on an array property

        $this->property = $property;
    }

    /**
     * @return ArtisanScaffoldProperty
     */
    public function getProperty () : ArtisanScaffoldProperty
    {
        return $this->property;
    }

    abstract public function getMigrationColumnSnippet() : string;
}