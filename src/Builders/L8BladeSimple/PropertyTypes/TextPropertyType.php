<?php
namespace Martinshaw\ArtisanScaffold\Builders\L8BladeSimple\PropertyTypes;

use Illuminate\Support\Str;
use Martinshaw\ArtisanScaffold\Console\Property\ArtisanScaffoldProperty;

class TextPropertyType extends L8BladeSimplePropertyType
{
    /**
     * TextPropertyType constructor.
     * @param ArtisanScaffoldProperty $property
     */
    public function __construct(ArtisanScaffoldProperty $property)
    {
        parent::__construct(
            'text',
            'Text',
            $property
        );
    }

    /**
     * @return string
     */
    public function getMigrationColumnSnippet(): string
    {
        $column = '            $table->string("' . $this->property->getName() . '")';
        $column .= in_array('required', $this->property->getModifiers()) ? '' : '->nullable()';
        $column .= in_array('unique', $this->property->getModifiers()) ? '->unique()' : '';
        return $column . ';';
    }

    /**
     * @return string
     */
    public function getFormInputSnippet(): string
    {
        $required = in_array('required', $this->getProperty()->getModifiers()) ? ' required="required" ' : '';
        $label = '<label for="'.$this->getProperty()->getName().'">'.Str::ucfirst(str_replace('_', ' ', Str::snake($this->getProperty()->getName()))).'</label>';
        $input = '<input id="'.$this->getProperty()->getName().'" name="'.$this->getProperty()->getName().'"'.$required.' />';
        return implode("\n", [
            '<div class="form-field">',
            '    ' . $label,
            '    ' . $input,
            '</div>',
        ]);
    }
}