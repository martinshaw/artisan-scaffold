<?php
namespace Martinshaw\ArtisanScaffold\Builders\L8BladeSimple\PropertyTypes;

abstract class L8BladeSimplePropertyType extends \Martinshaw\ArtisanScaffold\Console\PropertyTypes\PropertyType
{
    /**
     * @return string
     */
    abstract public function getFormInputSnippet () : string;
}