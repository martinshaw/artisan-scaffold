<?php
namespace Martinshaw\ArtisanScaffold\Builders\L8BladeSimple\PropertyTypes;

use Illuminate\Support\Str;
use Martinshaw\ArtisanScaffold\Console\Property\ArtisanScaffoldProperty;

class TextareaPropertyType extends L8BladeSimplePropertyType
{
    /**
     * TextareaPropertyType constructor.
     * @param ArtisanScaffoldProperty $property
     */
    public function __construct(ArtisanScaffoldProperty $property)
    {
        parent::__construct(
            'textarea',
            'Text Area',
            $property
        );
    }

    /**
     * @return string
     */
    public function getMigrationColumnSnippet(): string
    {
        $column = '            $table->string("' . $this->getProperty()->getName() . '")';
        $column .= in_array('required', $this->getProperty()->getModifiers()) ? '' : '->nullable()';
        $column .= in_array('unique', $this->getProperty()->getModifiers()) ? '->unique()' : '';
        return $column . ';';
    }

    /**
     * @return string
     */
    public function getFormInputSnippet(): string
    {
        $required = in_array('required', $this->getProperty()->getModifiers()) ? ' required="required" ' : '';
        $label = '<label for="'.$this->getProperty()->getName().'">'.Str::ucfirst(str_replace('_', ' ', Str::snake($this->getProperty()->getName()))).'</label>';
        $input = '<textarea id="'.$this->getProperty()->getName().'" name="'.$this->getProperty()->getName().'"'.$required.'></textarea>';
        return implode("\n", [
            '<div class="form-field">',
            '    ' . $label,
            '    ' . $input,
            '</div>',
        ]);
    }
}