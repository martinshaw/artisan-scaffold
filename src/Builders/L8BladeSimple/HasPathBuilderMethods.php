<?php
namespace Martinshaw\ArtisanScaffold\Builders\L8BladeSimple;

use Illuminate\Support\Str;

trait HasPathBuilderMethods
{
    /**
     * @param bool $createIfMissing
     * @return string
     */
    protected function getBladeViewsPath (bool $createIfMissing = false) : string
    {
        $viewsDir = base_path() . '/resources/views/' . Str::snake($this->context->getResourceNamePlural());
        if ($createIfMissing and !file_exists($viewsDir) and !is_dir($viewsDir)) {
            mkdir($viewsDir);
        }
        return $viewsDir;
    }

    /**
     * @return string
     */
    protected function getBladeWebRoutesPath () : string
    {
        return base_path() . '/routes/web.php';
    }

    /**
     * @param bool $createIfMissing
     * @return string
     */
    protected function getBladeWebRoutesStubPath (bool $createIfMissing = false) : string
    {
        $routesDir = base_path() . '/routes/web';
        if ($createIfMissing and !file_exists($routesDir) and !is_dir($routesDir)) {
            mkdir($routesDir);
        }
        return $routesDir . '/' . Str::snake($this->context->getResourceNamePlural()) . '.php';
    }
}