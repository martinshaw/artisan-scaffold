<?php
namespace Martinshaw\ArtisanScaffold\Builders\L8BladeSimple;

use Martinshaw\ArtisanScaffold\Builders\L8BladeSimple\PropertyTypes\L8BladeSimplePropertyType;
use Martinshaw\ArtisanScaffold\Console\Property\ArtisanScaffoldPropertiesManager;
use Illuminate\Support\Str;

class L8BladeSimpleBuilder extends \Martinshaw\ArtisanScaffold\Console\Builder\ArtisanScaffoldBuilder
{
    use HasPathBuilderMethods, HasSnippetBuilderMethods;

    /**
     * @inheritDoc
     */
    function run(ArtisanScaffoldPropertiesManager $propertiesManager, array $flags): void
    {
        $this->propertiesManager = $propertiesManager;

        $this->artisanMake();

        $this->handleMigration()
            ->handleWebController()
            ->handleViews()
            ->handleWebRoutes();

        $this->composerDumpAutoload();
    }

    protected function handleMigration () : self
    {
        $this->context->info('Updating migration file with columns for new properties ...');

        $this->writeFile(
            $this->getMigrationPath(),
            $this->getTemplateFile(__DIR__ . '/templates/migration.php', [
                'migrationColumns' => $this->getMigrationColumnSnippets(),
            ])
        );

        $this->context->line('');
        return $this;
    }

    protected function handleWebController () : self
    {
        $this->context->info('Updating web controller file with controller methods ...');

        $this->writeFile(
            $this->getWebControllerPath(),
            $this->getTemplateFile(__DIR__ . '/templates/web_controller.php')
        );

        $this->context->line('');
        return $this;
    }

    protected function handleViews () : self
    {
        $this->context->info('Adding blade view files ...');

        $this->writeFile(
            $this->getBladeViewsPath(true) . '/create.blade.php',
            $this->getTemplateFile(__DIR__ . '/templates/create_view.php', [
                'form' => $this->getFormInputSnippets(),
            ])
        );

        $this->writeFile(
            $this->getBladeViewsPath(true) . '/index.blade.php',
            $this->getTemplateFile(__DIR__ . '/templates/index_view.php')
        );

        $this->context->line('');
        return $this;
    }

    protected function handleWebRoutes () : self
    {
        $this->context->info('Adding web route definitions ...');

        $this->appendFile(
            $this->getBladeWebRoutesPath(),
            $this->getTemplateFile(__DIR__ . '/templates/web_route.php')
        );

        $this->writeFile(
            $this->getBladeWebRoutesStubPath(true),
            $this->getTemplateFile(__DIR__ . '/templates/web_route_stub.php')
        );

        $this->context->line('');
        return $this;
    }

    /**
     * @inheritDoc
     */
    function revert(array $flags): void
    {
        $this->context->info('Deleting migration file for ' . Str::studly($this->context->getResourceNamePlural()) . ' ...');
        $this->context->info('Deleting web controller file for ' . Str::studly($this->context->getResourceNamePlural()) . ' ...');
        $this->context->info('Deleting view files for ' . Str::studly($this->context->getResourceNamePlural()) . ' ...');

        $this->deleteFiles([
            $this->getMigrationPath(),
            $this->getWebControllerPath(),
            $this->getModelPath(),
            $this->getBladeWebRoutesStubPath(),
        ]);

        $this->deleteDirectories([
            $this->getBladeViewsPath()
        ]);
    }

    /**
     * @inheritDoc
     */
    public function getPropertyTypes (): array
    {
        return [
            'text' => \Martinshaw\ArtisanScaffold\Builders\L8BladeSimple\PropertyTypes\TextPropertyType::class,
            'textarea' => \Martinshaw\ArtisanScaffold\Builders\L8BladeSimple\PropertyTypes\TextareaPropertyType::class,
        ];
    }
}