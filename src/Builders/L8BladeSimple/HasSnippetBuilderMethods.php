<?php
namespace Martinshaw\ArtisanScaffold\Builders\L8BladeSimple;

use Martinshaw\ArtisanScaffold\Builders\L8BladeSimple\PropertyTypes\L8BladeSimplePropertyType;
use Illuminate\Support\Str;

trait HasSnippetBuilderMethods
{
    /**
     * @return string
     */
    protected function getFormInputSnippets () : string
    {
        $pre = [
            '<form action="{{route(\''.Str::snake($this->context->getResourceNameSingular()).'.store\')}}" method="POST">',
            '    @csrf',
        ];
        $post = [
            '    <input type="submit" value="Create" />',
            '</form>',
        ];
        return implode(
            "\n",
            array_merge(
                $pre,
                array_map(function (L8BladeSimplePropertyType $property) {
                    return $property->getFormInputSnippet();
                }, $this->propertiesManager->getProperties()),
                $post
            )
        );
    }
}