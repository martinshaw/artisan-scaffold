<?php
/**
 * @var string $resourceSingularUpper
 * @var string $resourceSingularLower
 * @var string $resourcePluralUpper
 * @var string $resourcePluralLower
 * @var string $firstPropertyName
 * @var string $firstPropertyCaption
 * @var array $properties
 */

$output = <<<PHP
<?php
namespace App\Http\Controllers;

use App\Models\\{$resourceSingularUpper};
use Illuminate\Http\Request;

class {$resourceSingularUpper}Controller extends Controller
{
    public function index()
    {
        return view('{$resourcePluralLower}.index', [
            '{$resourcePluralLower}' => {$resourceSingularUpper}::all(),
        ]);
    }
    
    public function create()
    {
        return view('{$resourcePluralLower}.create');
    }
    
    public function store()
    {
    
    }
    
    public function show(int \$id)
    {
        return view('{$resourcePluralLower}.show', [
            '{$resourceSingularLower}' => {$resourceSingularUpper}::findOrFail(\$id),
        ]);
    }
    
    public function edit()
    {
        return view('{$resourcePluralLower}.edit', [
            '{$resourceSingularLower}' => {$resourceSingularUpper}::findOrFail(\$id),
        ]);
    }
    
    public function update()
    {
    
    }
    
    public function destroy()
    {
        
    }
}
PHP;

return $output;
