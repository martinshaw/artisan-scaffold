<?php
/**
 * @var string $resourceSingularUpper
 * @var string $resourceSingularLower
 * @var string $resourcePluralUpper
 * @var string $resourcePluralLower
 * @var string $firstPropertyName
 * @var string $firstPropertyCaption
 * @var array $properties
 * @var array $migrationColumns
 */

$output = <<<PHP
<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Create{$resourcePluralUpper}Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('{$resourcePluralLower}', function (Blueprint \$table) {
            \$table->id();
{$migrationColumns}
            \$table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('{$resourcePluralLower}');
    }
}
PHP;

return $output;