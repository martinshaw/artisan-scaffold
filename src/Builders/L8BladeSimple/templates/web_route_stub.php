<?php
/**
 * @var string $resourceSingularUpper
 * @var string $resourceSingularLower
 * @var string $resourcePluralUpper
 * @var string $resourcePluralLower
 * @var string $firstPropertyName
 * @var string $firstPropertyCaption
 * @var array $properties
 */

$output = <<<PHP
<?php

use App\Http\Controllers\\{$resourceSingularUpper}Controller;

Route::prefix('{$resourcePluralLower}')->name('{$resourceSingularLower}.')->group(function () {
    Route::get('/', [{$resourceSingularUpper}Controller::class, 'index'])->name('index');
    Route::get('/create', [{$resourceSingularUpper}Controller::class, 'create'])->name('create');
    Route::post('/', [{$resourceSingularUpper}Controller::class, 'store'])->name('store');
    Route::get('/{{$resourceSingularLower}}/edit', [{$resourceSingularUpper}Controller::class, 'edit'])->name('edit');
    Route::put('/{{$resourceSingularLower}}', [{$resourceSingularUpper}Controller::class, 'update'])->name('update');
    Route::get('/{{$resourceSingularLower}}', [{$resourceSingularUpper}Controller::class, 'show'])->name('show');
    Route::delete('/{{$resourceSingularLower}}', [{$resourceSingularUpper}Controller::class, 'destroy'])->name('destroy');
});

PHP;

return $output;