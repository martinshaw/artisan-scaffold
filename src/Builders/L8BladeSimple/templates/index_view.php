<?php
/**
 * @var string $resourceSingularUpper
 * @var string $resourceSingularLower
 * @var string $resourcePluralUpper
 * @var string $resourcePluralLower
 * @var string $firstPropertyName
 * @var string $firstPropertyCaption
 * @var array $properties
 */

$output = <<<BLADE
<html>
    <head>
        <title>{$resourcePluralUpper}</title>
    </head>
    <body>
        <h1>{$resourcePluralUpper}</h1>
        
        <table>
            <tr>
                <th>{$resourceSingularUpper} {$firstPropertyCaption}</th>
                <th>Actions</th>
            </tr>
            @foreach (\${$resourcePluralLower} as \${$resourceSingularLower})
            <tr>
                <td>{{ \${$resourceSingularLower}->{$firstPropertyName} }}</td>
                <td>
                    <a href="{{ route('{$resourceSingularLower}.show') }}">Show</a> - 
                    <a href="{{ route('{$resourceSingularLower}.edit') }}">Edit</a> - 
                    <form action="{{ route('{$resourceSingularLower}.destroy') }}" method="POST">
                        @csrf
                        @method('DELETE')
                        <a href="#" onclick="event.preventDefault(); this.parentElement.submit();">Delete</a>
                    </form>
                </td>
            </tr>
            @endforeach
            <tr>
                <td><a href="{{ route('{$resourceSingularLower}.create') }}"><i>Create new {$resourceSingularUpper}</i></a></td>
                <td>&nbsp;</td>
            </tr>
        </table>
    </body>
</html>
BLADE;

return $output;
