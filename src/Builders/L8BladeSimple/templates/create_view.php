<?php
/**
 * @var string $resourceSingularUpper
 * @var string $resourceSingularLower
 * @var string $resourcePluralUpper
 * @var string $resourcePluralLower
 * @var string $firstPropertyName
 * @var string $firstPropertyCaption
 * @var array $properties
 * @var string $form
 */

$output = <<<BLADE
<html>
    <head>
        <title>{$resourcePluralUpper}</title>
    </head>
    <body>
        <h1>{$resourcePluralUpper}</h1>
        
        {$form}
    </body>
</html>
BLADE;

return $output;
