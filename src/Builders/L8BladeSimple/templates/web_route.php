<?php
/**
 * @var string $resourceSingularUpper
 * @var string $resourceSingularLower
 * @var string $resourcePluralUpper
 * @var string $resourcePluralLower
 * @var string $firstPropertyName
 * @var string $firstPropertyCaption
 * @var array $properties
 */

$output = <<<PHP
require 'web/{$resourcePluralLower}.php';

PHP;

return $output;