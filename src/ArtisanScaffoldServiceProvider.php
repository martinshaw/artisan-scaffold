<?php

namespace Martinshaw\ArtisanScaffold;

use Illuminate\Support\ServiceProvider;
use Martinshaw\ArtisanScaffold\Console\Commands\ArtisanScaffoldRunCommand;
use Martinshaw\ArtisanScaffold\Console\Commands\ArtisanScaffoldRevertCommand;

class ArtisanScaffoldServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     */
    public function boot()
    {
        if ($this->app->runningInConsole()) {
            // Publishing the configs.
            $this->publishes([
                __DIR__.'/../config/config.php' => config_path('artisan-scaffold.php'),
            ], 'config');

            // Registering package commands.
            $this->commands([
                ArtisanScaffoldRunCommand::class,
                ArtisanScaffoldRevertCommand::class,
            ]);
        }
    }

    /**
     * Register the application services.
     */
    public function register()
    {
        //
    }
}
