<?php

return [
    'default_builder' => 'l8-blade-simple',

    'builders' => [
        'l8-blade-simple' => \Martinshaw\ArtisanScaffold\Builders\L8BladeSimple\L8BladeSimpleBuilder::class,
    ],
];